int balleX; 
int balleY;
int plateformeX;
int plateformeY;
int deplacementX;
int deplacementY;

void setup()
{

   plateformeX = 15;
   plateformeY = 60;
   deplacementX = 6;
   deplacementY = -3;
   balleY = 200;
   balleX = 200;
   size(400,400);
   background(0);
}

void draw() 
{
   background(0); 
   smooth();
   fill(255);
   rect(plateformeX, plateformeY, 25, 85); 
   ellipse(balleX, balleY, 20, 20); 

   balleX = balleX + deplacementX;
   balleY = balleY + deplacementY; 
   plateformeY = (mouseY);

 
   if (balleX >  width-10 && deplacementX > 0)
   {
      deplacementX = -deplacementX;
   }
   
  
   if (balleY > height+10 && deplacementY > 0)
   {
      deplacementY = -deplacementY;
   }

 
   if (balleY < 10 && deplacementY < 0) 
   {
     deplacementY = abs(deplacementY);
   }

  
   if (balleX < plateformeX+35 && balleY > plateformeY && balleY < plateformeY+85) 
   {
      deplacementX = -deplacementX;
   }

  
   if (balleX < 10) 
   {
      noLoop();
      background(255, 0, 0);
   }
}
